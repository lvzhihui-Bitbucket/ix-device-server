
#ifndef  _IXDeviceServer_H_
#define  _IXDeviceServer_H_

#include "main.h"
#include "dbaccounts.h"
#include "threadpool.h"

#define	TEST_USER_ID			"VTKRemoteTest"
#define	TEST_USER_PSD			"02038629088"

#define UNREGISTER_ACCOUNT 		"UNREGISTERACCOUNT"

#define ACCOUNT_TYPE_MONITOR	0
#define ACCOUNT_TYPE_APP		1
#define ACCOUNT_TYPE_MAX		2

#define NORMAL_LOGIN_REQ			0x1000
#define NORMAL_LOGIN_RSP			0x1001
#define PROXY_ONLINE_REQ			0x1002
#define PROXY_ONLINE_RSP			0x1003
#define PC_BIND_PROXY_REQ			0x1004
#define PC_BIND_PROXY_RSP			0x1005
#define NORMAL_BEAT_HEART_REQ		0x1006
#define NORMAL_BEAT_HEART_RSP		0x1007
#define NORMAL_TRANSFER_DAT			0x1008

#define SERVER_NOTIFY2DEVICE_TYPE	0x2000
#define INNER_MSG_CMD_TYPE			0x6000
#define INNER_MSG_TIMING_1S				1
#define INNER_MSG_CLIENT_CLOSED			2
#define INNER_MSG_CLIENT_ERROR			3
#define INNER_MSG_CLIENT_ERROR_OTHERS	4

#pragma pack(push)
#pragma pack(2)

#define 	HEAD_FLAG_1		'V'
#define 	HEAD_FLAG_2		'T'
#define 	HEAD_FLAG_3		'E'
#define 	HEAD_FLAG_4		'K'

struct COMMU_PACK_HEAD
{
	char	head[4];
	short	cmd_type;
	short	cmd_id;
	short	dev_type;
	short 	dat_len;
	COMMU_PACK_HEAD() 
	{ 
		head[0]=HEAD_FLAG_1;
		head[1]=HEAD_FLAG_2;
		head[2]=HEAD_FLAG_3;
		head[3]=HEAD_FLAG_4; 
	};
};

struct COMMU_PACK_NORMAL_LOGIN_REQ
{
	COMMU_PACK_HEAD		head;
	short				login;			// 0/logout, 1/login
	char				userid[32];
	char				password[16];
	short				reserve;
};
struct COMMU_PACK_NORMAL_LOGIN_RSP
{
	COMMU_PACK_HEAD		head;
	short				login;			// 0/logout, 1/login
	char				userid[32];
	char				password[16];
	short				reserve;
	short				result;			// 0/ok, 1/err
};

struct COMMU_PACK_NORMAL_ONLINE_REQ
{
	COMMU_PACK_HEAD		head;
	short				online;			// 0/offline, 1/online
	char				account[32];
	char				password[16];
	short				reserve;
};
struct COMMU_PACK_NORMAL_ONLINE_RSP
{
	COMMU_PACK_HEAD		head;
	short				online;			// 0/offline, 1/online
	char				account[32];
	char				password[16];
	short				reserve;
	short				result;			// 0/ok, 1/err
};

struct COMMU_PACK_NORMAL_BIND_REQ
{
	COMMU_PACK_HEAD		head;
	short				bind;			// 0/unbind, 1/bind
	char				account[32];
	char				password[16];
	short				reserve;
};
struct COMMU_PACK_NORMAL_BIND_RSP
{
	COMMU_PACK_HEAD		head;
	short				bind;			// 0/unbind, 1/bind
	char				account[32];
	char				password[16];
	short				reserve;
	short				result;			// 0/ok, 1/err
};

struct COMMU_PACK_NORMAL_BEAT_HEART_REQ
{
	COMMU_PACK_HEAD		head;
	int					id;
	short				reserve;
};

struct COMMU_PACK_NORMAL_BEAT_HEART_RSP
{
	COMMU_PACK_HEAD		head;
	int					id;
	short				reserve;
};

struct COMMU_PACK_NORMAL_TRANSFER_DAT
{
	COMMU_PACK_HEAD		head;
	char				dat[2000];
};

struct COMMU_PACK_SERVER_NOTIFY
{
	COMMU_PACK_HEAD		head;
	short				notify_cmd;
	short				notify_dat;
};

#pragma pack(pop)

struct LibeventThread
{
	pthread_t 			tid;	
	struct event_base 	*base;	
	struct event 		notifyEvent;
	int 				notifyReceiveFd;
	int 				notifySendFd;	
	CIXDeviceServer* 	pserver;
};

struct inner_msg_data
{
	int					type;	// 0/innner create, 1/ext received data
	CMyClientNode* 		conn;
	int					rcv_len;	
	char				*rcv_pbuf;
};

class CIXDeviceServer
{
private:
	int m_threads_cnt;		
	int m_port;				
	LibeventThread *m_MainThread;
	LibeventThread *m_SubThreads;

	struct	event 	*ctrlc_ev;
	struct 	event	*timeout_ev;

	static void *sub_thread_libevent(void *arg);
    static void timeout_cb(int id, short events, void *arg);  
	static void quit_cb(int sig, short events, void *arg); 
public:	
	CIXDeviceServer(int count);
	~CIXDeviceServer();

	CDBAcounts		m_all_node;

	static const int EXIT_CODE = -1;
	static void one_connect_process( int fd, short which, void *arg );
	static void listenner_event_cb(evconnlistener *listener, evutil_socket_t fd, sockaddr *sa, int socklen, void *user_data);

	void setup_thread(LibeventThread *me);

	void set_port( int port )	{ m_port = port; }	
	bool start_run();
	void stop_run(timeval *tv);

	static void RcvBufProcess(CMyClientNode* conn, char* rd_buf, int len); 

	int							queue_thread_en;
	pthread_t 					queue_thread_tid;
	pthread_mutexattr_t 		queue_thread_attr;
	pthread_mutex_t 			queue_thread_lock; 
	pthread_cond_t 				queue_thread_cond;	
	queue<inner_msg_data*>   	queue_dat;	

	static int push_inner_msg_rcv_queue(  void *arg, char* pbuf, int len, CMyClientNode *owner, int ext_type );
	static inner_msg_data* pop_inner_msg_rcv_queue( void *arg );	
	static void clean_inner_msg_rcv_queue_thread(void * arg);	
	static void* inner_msg_dat_process(void *arg);
	
	void start_inner_msg_rcv_queue_thread(void);
	void stop_inner_msg_rcv_queue_thread(void);
};


#endif
