
#ifndef _MAIN_H
#define _MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include <event.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>
#include <event2/thread.h>

#include <iostream>
#include <string>  

#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#include "myEventClient.h"
#include "dbqueue.h"

class CDBAcounts;
class CIXDeviceServer;
class CMyClientNode;
struct LibeventThread;

#define UBUNTU_HOST
#ifdef UBUNTU_HOST
	#include <syslog.h>     
	#define	mmp_printf(fmt,...)	syslog(LOG_INFO, ""fmt"",##__VA_ARGS__)
#else
	#define	mmp_printf(fmt,...) printf(""fmt"",##__VA_ARGS__)
#endif

//#define VIDEO_STREAM_WRITE2FILE

#define NODE_DATA_PROCESS_TYPE1		0			// all nodes use one public buffer, process data step one by one with one thresd
#define NODE_DATA_PROCESS_TYPE2		1			// all nodes use one queue itself, process data in queue with thread pool
#define NODE_DATA_PROCESS_TYPE3		2			// all nodes not use buffer, process data directly with event base threads

#define NODE_DATA_PROCESS_TYPE		NODE_DATA_PROCESS_TYPE3		

#endif


