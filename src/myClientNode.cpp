
#include "main.h"
#include "myClientNode.h"
#include "dbaccounts.h"
#include "IXDeviceServer.h"

void CMyClientNode::relate_lock()
{
	pthread_mutex_lock(&m_relation_lock);	
}

void CMyClientNode::relate_unlock()
{
	pthread_mutex_unlock(&m_relation_lock);	
}

//  0:ok, 1:full, -1:existed
int CMyClientNode::inc_active_related_conn_ptr(CMyClientNode* pconn)
{
	int i;
	int ret = -1;
	//pthread_mutex_lock(&m_relation_lock);
	for( i = 0; i < m_active_relating_conn_cnt; i++ )
	{
		if( m_active_relating_conns[i] == pconn )
			break;
	}
	if( i == m_active_relating_conn_cnt )
	{
		if( m_active_relating_conn_cnt < max_conns_of_account_to_relate )
		{
			m_active_relating_conns[m_active_relating_conn_cnt++] = pconn;
			ret = 0;
		}
		else
			ret = 1;
	}
	//pthread_mutex_unlock(&m_relation_lock);
	return ret;
}

// 0:ok, 1:empty, -1:not exist
int CMyClientNode::dec_active_related_conn_ptr(CMyClientNode* pconn)
{
	int i;
	int ret = -1;
	//pthread_mutex_lock(&m_relation_lock);
	if( !m_active_relating_conn_cnt )
		ret = 1;
	for( i = 0; i < m_active_relating_conn_cnt; i++ )
	{
		if( m_active_relating_conns[i] == pconn )
			break;
	}
	if( i < m_active_relating_conn_cnt )
	{
		m_active_relating_conn_cnt--;
		for( ; i < m_active_relating_conn_cnt; i++ )
		{
			m_active_relating_conns[i] = m_active_relating_conns[i+1];
		}
		ret = 0;
	}
	//pthread_mutex_unlock(&m_relation_lock);
	return ret;
}

CMyClientNode* CMyClientNode::get_active_relating_conn_ptr(int cnt)
{
	CMyClientNode* pconn;
	//pthread_mutex_lock(&m_relation_lock);
	if( cnt >= m_active_relating_conn_cnt )
		pconn = NULL;
	else
		pconn = m_active_relating_conns[cnt];
	//pthread_mutex_unlock(&m_relation_lock);
	return pconn;	
}

int CMyClientNode::get_active_related_conn_cnt()
{
	int conn_cnt;
	//pthread_mutex_lock(&m_relation_lock);
	conn_cnt = m_active_relating_conn_cnt;	
	//pthread_mutex_unlock(&m_relation_lock);	
	return conn_cnt;
}
/*---------------------------------------------------------------------------------------------------*/
queue<DAT_BUFF*>* CMyClientNode::get_type0_write_vec_ptr()
{
	queue<DAT_BUFF*>* ptr;
	//pthread_mutex_lock(&m_rdbuf_lock);
	if( m_type0_vec_w_ind )
		ptr = &m_type0_vec1;
	else
		ptr = &m_type0_vec0;		
	//pthread_mutex_unlock(&m_rdbuf_lock);
	return ptr;
}

queue<DAT_BUFF*>* CMyClientNode::get_type0_read_vec_ptr()
{
	queue<DAT_BUFF*>* ptr;
	//pthread_mutex_lock(&m_rdbuf_lock);
	if( m_type0_vec_w_ind )
		ptr = &m_type0_vec0;
	else
		ptr = &m_type0_vec1;	
	//pthread_mutex_unlock(&m_rdbuf_lock);
	return ptr;
}

void CMyClientNode::switch_type0_rd_vec_ptr()
{
	//pthread_mutex_lock(&m_rdbuf_lock);
	m_type0_vec_w_ind = !m_type0_vec_w_ind;
	//pthread_mutex_unlock(&m_rdbuf_lock);
}

int CMyClientNode::clear_one_queue(queue<DAT_BUFF*>* ptr)
{
	pthread_mutex_lock(&m_rdbuf_lock);
	DAT_BUFF* pnode;
	while( !(*ptr).empty() )
	{
		pnode = (*ptr).front();
		if( pnode != NULL )
		{
			if( pnode->pdat != NULL )
			{
				delete []pnode->pdat;
				pnode->pdat = NULL;
			}
			delete pnode;
			pnode = NULL;
		}			
		(*ptr).pop();
	}
	pthread_mutex_unlock(&m_rdbuf_lock);
	return 0;
}

int CMyClientNode::copy_my_queue(queue<DAT_BUFF*>* ptr_target)
{
	pthread_mutex_lock(&m_rdbuf_lock);
	
	queue<DAT_BUFF*>* ptr_source = get_type0_read_vec_ptr();	
	// copy from source to target
	DAT_BUFF* pnode_s;
	DAT_BUFF* pnode_t;
	int size = (*ptr_source).size();
	for( int i = 0; i < size; i++ )
	{
		pnode_s = (*ptr_source).front();
		// new node, copy data
		pnode_t 		= new DAT_BUFF;
		pnode_t->len	= pnode_s->len;
		pnode_t->pdat 	= new char[pnode_s->len];
		memcpy( pnode_t->pdat, pnode_s->pdat, pnode_s->len);
		ptr_target->push(pnode_t);
		// notice: queue not support BIAN-LI,then pop followed push
		(*ptr_source).pop();
		(*ptr_source).push(pnode_s);
	}
	
	pthread_mutex_unlock(&m_rdbuf_lock);	
	return 0;
}

int CMyClientNode::write_local_buff(int media_type, char* pdat,int len)
{
	if( media_type == 0 )
	{
		pthread_mutex_lock(&m_rdbuf_lock);
	
		queue<DAT_BUFF*> *ptr_queue = get_type0_write_vec_ptr();
		// new node, copy data
		DAT_BUFF* pnode = new DAT_BUFF;
		pnode->len	= len;
		pnode->pdat = new char[len];
		memcpy( pnode->pdat, pdat, len);
		// push into the queue
		ptr_queue->push(pnode);
		
		pthread_mutex_unlock(&m_rdbuf_lock);
		
		// just type0 pack head and have no device read the reader vector, then switch writer ping pang buffer
		MEDIA_TYPE0_SEND_STRU* ptr_dat = (MEDIA_TYPE0_SEND_STRU*)pdat;
		if( (ptr_dat->pack_id == -1) || (ptr_dat->pack_id == (ptr_dat->total_pack-1)) )
		{
			switch_type0_rd_vec_ptr();	
			ptr_queue = get_type0_write_vec_ptr();				
			clear_one_queue(ptr_queue);
		}
	}
	return 0;
}

int CMyClientNode::clear_local_buff(int media_type)
{
        if( media_type == 0 )
        {
				//pthread_mutex_lock(&m_rdbuf_lock);
                queue<DAT_BUFF*> *ptr_queue = get_type0_write_vec_ptr();
				clear_one_queue(ptr_queue);
				//pthread_mutex_unlock(&m_rdbuf_lock);
        }
        return 0;
}




int CMyClientNode::is_send_buff_empty(void)
{
	return (m_type0_send_vec.size()==0)?1:0;
}

int CMyClientNode::clear_send_buff(void)
{
	clear_one_queue(&m_type0_send_vec);
	return 0;
}

int CMyClientNode::update_send_buff(CMyClientNode* ptr_dat_source,int media_type)
{
	ptr_dat_source->copy_my_queue(&m_type0_send_vec);
	return 0;
}

int CMyClientNode::pop_send_buff(char* pdat, int* plen)
{
	pthread_mutex_lock(&m_rdbuf_lock);

	int size = m_type0_send_vec.size();
	if( size == 0 )
	{
		pthread_mutex_unlock(&m_rdbuf_lock);	
		return 0;
	}
	DAT_BUFF* pnode = m_type0_send_vec.front();
	*plen = pnode->len;
	memcpy( pdat, pnode->pdat, pnode->len );
	delete []pnode->pdat;
	pnode->pdat = NULL;
	delete pnode;
	pnode = NULL;
	m_type0_send_vec.pop();		
	
	pthread_mutex_unlock(&m_rdbuf_lock);		
	return size;
}


int CMyClientNode::server_notify_device_nat_ok(void)
{
	struct COMMU_PACK_SERVER_NOTIFY pack;
	pack.head.cmd_type 	= SERVER_NOTIFY2DEVICE_TYPE;
	pack.head.dev_type	= m_dev_type;
	pack.notify_cmd		= 0x0001;
	pack.notify_dat		= 0x0000;
	m_evtcomm.SendWriteBuffer(	(char*)&pack, sizeof(struct COMMU_PACK_SERVER_NOTIFY));
	return 0;
}

int CMyClientNode::store_received_data(char* pbuf, int len)
{
	if( store_zone == NULL )
	{
		store_len = len;
		store_zone = new char[store_len];
		memcpy(store_zone,pbuf,store_len);
	}
	else
	{
		char* pnew = new char[len+store_len];
		memcpy(pnew,store_zone,store_len);
		memcpy(pnew+store_len,pbuf,len);
		delete []store_zone;
		store_zone = pnew;
		store_len += len;
	}
	return store_len;
}

void* CMyClientNode::probe_received_data(int *plen)
{
	*plen = store_len;
	return (void*)store_zone;
}

int CMyClientNode::dump_received_data(char* pbuf, int len)
{
	if( len <= store_len )
	{
		memcpy(pbuf,store_zone,len);
		store_len -= len;
		if( store_len == 0 )
		{
			delete []store_zone;
			store_zone = NULL;
		}
		else
		{
			char* pnew = new char[store_len];
			memcpy(pnew, store_zone+len,store_len);
			delete []store_zone;
			store_zone = pnew;
		}
		return len;
	}
	else
		return 0;
}
