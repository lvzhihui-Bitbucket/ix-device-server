
#ifndef _DBLIST_
#define _DBLIST_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <string>  
using std::string;

#include <queue>
using std::queue;

struct DAT_BUFF
{
	int		len;		// 数据的长度
	char*	pdat;		// 数据包的地址
};

struct PTR_DAT_BUFF
{
	void*	ptr;		// obj ptr
	int		len;		// 数据的长度
	char*	pdat;		// 数据包的地址
};

// FIFO queue
class CDataQueue
{
public:	
    CDataQueue();
    ~CDataQueue();

	pthread_mutex_t     m_lock;

	queue<DAT_BUFF*>    	m_dat_queue;    
	int clear_the_queue();
	int copy_to_queue(CDataQueue* ptr_target);
    int push_into_queue(char* pdat,int len);
    int pop_from_queue(char* pdat, int* plen);

	queue<PTR_DAT_BUFF*>    m_ptr_dat_queue; 
	int clear_the_ptr_queue();
	int copy_to_ptr_queue(CDataQueue* ptr_target);
    int push_into_ptr_queue(void* ptr, char* pdat,int len);
    int pop_from_ptr_queue(void** pptr, char* pdat, int* plen);
};

#endif
