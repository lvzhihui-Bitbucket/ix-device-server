/*
[2021-05-18]
New Project, copy from VTKMediaServer and modified!

*/

#include "main.h"

#include "IXDeviceServer.h"
#include "myEventClient.h"
#include "dbqueue.h"
#include "dbaccounts.h"
#include "threadpool.h"

#include "mySqlConnect.h"

#ifdef UBUNTU_HOST	

void sigroutine(int dunno)
{
	switch (dunno) 
	{
		case SIGKILL:
		mmp_printf("Get a signal -- SIGKILL ");
		break;
		case SIGTERM:
		mmp_printf("Get a signal -- SIGTERM ");
		break;
	}
	return;
}

#endif

int main( int argc, char* argv[] ) 
{
#ifdef UBUNTU_HOST	
	signal(SIGTTOU,SIG_IGN); 
	signal(SIGTTIN,SIG_IGN); 
	signal(SIGCHLD,SIG_IGN);

	signal(SIGKILL ,sigroutine); 
	signal(SIGTERM, sigroutine);

	daemon(0,0);
	openlog(argv[0],LOG_CONS | LOG_PID, LOG_USER);
#endif

	mmp_printf("pid: %d\n", getpid());

	CIXDeviceServer server(5);
	server.set_port(8860);

	server.start_run();

	mmp_printf("done\n");
}

