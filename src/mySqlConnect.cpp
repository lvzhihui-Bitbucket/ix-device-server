


#include <stdio.h>
#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <stdexcept>
 
using namespace std;
 
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h> 
#include <cppconn/resultset.h> 
#include <cppconn/statement.h> 
#include <cppconn/prepared_statement.h>

#include<boost/version.hpp>
#include<boost/config.hpp>
 
using namespace sql;

void RunConnectMySQL()   
{
    cout << BOOST_VERSION << endl;
    cout << BOOST_LIB_VERSION << endl;
    cout << BOOST_PLATFORM << endl;
    cout << BOOST_COMPILER << endl;
    cout << BOOST_STDLIB << endl;

    mysql::MySQL_Driver *driver;  
    Connection *con;  
    Statement *state;  
    ResultSet *result;  
    // 初始化驱动  
    driver = sql::mysql::get_mysql_driver_instance();  
    // 建立链接  
    con = driver->connect("tcp://127.0.0.1:3306", "root", "1");  
    state = con->createStatement();  
    state->execute("use opensips");  
    // 查询  
    result = state->executeQuery("select * from subscriber where id < 1002");  
    // 输出查询  
    while( result->next() )  
    {  
        int id = result->getInt("id");  
        string name = result->getString("username");  
        cout << id << " : " << name << endl;  
    }  
    delete state;  
    delete con;  
}  


#define DBHOST      "tcp://127.0.0.1:3306"
#define USER        "root"
#define PASSWORD    "1"

#define SEARCH_ONE_ACCOUNT3_WITH_PSW "SELECT * from subscriber where (username='%s') and (password='%s');"

int check_account_password(string account, string pasword)
{
    Driver *driver;
    Connection *conn;
    Statement *state;  
    ResultSet *result;  

    driver = get_driver_instance();
    conn = driver->connect(DBHOST, USER, PASSWORD);
    conn->setAutoCommit(0);
    cout<<"DataBase connection autocommit mode = "<<conn->getAutoCommit()<<endl;
    // use opensips table
    state = conn->createStatement();  
    state->execute("use opensips");  
    // query
    int query_cnt = 0;
	char query_str[400]; 
    sprintf(query_str, SEARCH_ONE_ACCOUNT3_WITH_PSW, account.c_str(), pasword.c_str());	
    result = state->executeQuery(query_str);      
    while( result->next() )  
    {  
        int id = result->getInt("id");  
        string usr = result->getString("username");  
        string psw = result->getString("password");  
        cout << id << " : " << usr << "(" << psw <<")" << endl;    
        query_cnt++;      
    }    
    delete state; 
    delete conn;
    driver = NULL;
    conn = NULL;
    return query_cnt;
}
