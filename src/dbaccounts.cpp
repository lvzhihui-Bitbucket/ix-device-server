
#include "main.h"
#include "dbaccounts.h"
#include "IXDeviceServer.h"
#include "myClientNode.h"

CDBAcounts::CDBAcounts()
{
    pthread_mutex_init( &m_unregister_lock,0);
    pthread_mutex_init( &m_registered_lock,0);
    pthread_mutex_init( &m_tobekilled_lock,0);
}

CDBAcounts::~CDBAcounts()
{
    unregister_del_all_account_node();
	registered_del_all_account_node();
    tobekilled_del_all_account_node();

	pthread_mutex_destroy(&m_unregister_lock);
	pthread_mutex_destroy(&m_registered_lock);
	pthread_mutex_destroy(&m_tobekilled_lock);

    mmp_printf("<<CDBAcounts::~CDBAcounts>>\n");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
int CDBAcounts::tobekilled_add_one_account_node( CMyClientNode* ptr )
{
	pthread_mutex_lock(&m_tobekilled_lock);
    mmp_printf("5.<<tobekilled_add_one_account_node>>[ptr=%p,m_account=%s]\n",ptr,ptr->m_account.c_str());
	ptr->set_conn_state_TOBEKILLED();
    m_accounts_tobekilled.insert(make_pair(ptr, ptr->m_account));
	mmp_printf("5.>>>TOBEKILLED ADD:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_tobekilled.size());
	pthread_mutex_unlock(&m_tobekilled_lock);
    return 0;
}

int CDBAcounts::tobekilled_del_one_account_node( CMyClientNode* ptr )
{
	pthread_mutex_lock(&m_tobekilled_lock);
	
    MAP_PTR2STR::iterator it = m_accounts_tobekilled.find(ptr);
    if( it != m_accounts_tobekilled.end() )
    {
        mmp_printf("7.<<tobekilled_del_one_account_node>>[ptr=%p,m_account=%s]\n",ptr,ptr->m_account.c_str());
        delete ptr;
        m_accounts_tobekilled.erase(it);
    }
	pthread_mutex_unlock(&m_tobekilled_lock);
    return 0;
}

int CDBAcounts::tobekilled_del_all_account_node()
{
	pthread_mutex_lock(&m_tobekilled_lock);
	MAP_PTR2STR::iterator   it;
	it = m_accounts_tobekilled.begin();
	while( it != m_accounts_tobekilled.end() )
	{
        delete it->first;
        m_accounts_tobekilled.erase(it++);
	}
	pthread_mutex_unlock(&m_tobekilled_lock);
    return 0;
}

int CDBAcounts::tobekilled_timeout_process()
{
	//mmp_printf(">>>tobekilled_timeout_processs 1<<<\n");
	pthread_mutex_lock(&m_tobekilled_lock);
	string account;
	CMyClientNode* pnode;
	MAP_PTR2STR::iterator   it;
	it = m_accounts_tobekilled.begin();
	//mmp_printf(">>>tobekilled_timeout_processs 2<<<\n");
	while( it != m_accounts_tobekilled.end() )
	{
        pnode	= it->first;
        account = it->second;
		if( pnode->state_keep_above(5) )
		{
            delete pnode;
			m_accounts_tobekilled.erase(it++);	// user it++ 
			mmp_printf("6.>>>TOBEKILLED TIMEOUT:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_tobekilled.size());
		}
		else
			it++;
	}
	//mmp_printf(">>>tobekilled_timeout_processs 3<<<\n");	
	pthread_mutex_unlock(&m_tobekilled_lock);
	//mmp_printf(">>>tobekilled_timeout_processs 4<<<\n");
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
int CDBAcounts::unregister_add_one_account_node( CMyClientNode* ptr )
{
	pthread_mutex_lock(&m_unregister_lock);
    mmp_printf("1.<<unregister_add_one_account_node>>[ptr=%p,m_account=%s]\n",ptr,ptr->m_account.c_str());
	ptr->set_conn_state_UNREGISTER();
    m_accounts_unregister.insert(make_pair(ptr, ptr->m_account));
	mmp_printf("1.>>>UNREGISTER ADD:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_unregister.size());
	pthread_mutex_unlock(&m_unregister_lock);
    return 0;
}

int CDBAcounts::unregister_del_one_account_node( CMyClientNode* ptr, bool del_node )
{
	pthread_mutex_lock(&m_unregister_lock);
	
    MAP_PTR2STR::iterator it = m_accounts_unregister.find(ptr);
    if( it != m_accounts_unregister.end() )
    {
        mmp_printf("2.<<unregister_del_one_account_node>>[ptr=%p,m_account=%s]\n",ptr,ptr->m_account.c_str());
        if( del_node ) delete ptr;
        m_accounts_unregister.erase(it);
    }
	pthread_mutex_unlock(&m_unregister_lock);
    return 0;
}

int CDBAcounts::unregister_del_all_account_node()
{
	pthread_mutex_lock(&m_unregister_lock);
	MAP_PTR2STR::iterator   it;
	it = m_accounts_unregister.begin();
	while( it != m_accounts_unregister.end() )
	{
        delete it->first;
        m_accounts_unregister.erase(it++);
	}
	pthread_mutex_unlock(&m_unregister_lock);
    return 0;
}

int CDBAcounts::unregister_timeout_process()
{
	pthread_mutex_lock(&m_unregister_lock);
	string account;
	CMyClientNode* pnode;
	MAP_PTR2STR::iterator   it;
	it = m_accounts_unregister.begin();
	while( it != m_accounts_unregister.end() )
	{
        pnode	= it->first;
        account = it->second;

		//if( pnode->state_keep_above(10) )
		if( pnode->state_keep_above(120) )	// user need time to enter talk when notify is coming
		{
            delete pnode;
			m_accounts_unregister.erase(it++);
			mmp_printf(">>>UNREGISTER TIMEOUT:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_unregister.size());
		}
		else
		{
			it++;
		}
	}
	pthread_mutex_unlock(&m_unregister_lock);
    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int CDBAcounts::registered_add_one_account_node( CMyClientNode* ptr )
{
    mmp_printf("3.<<registered_add_one_account_node>>[ptr=%p,m_account=%s]\n",ptr,ptr->m_account.c_str());
	ptr->set_conn_state_REGISTERED();
    m_accounts_registered.insert(make_pair(ptr->m_account, ptr));
	mmp_printf("3.>>>REGISTERED ADD:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_registered.size());
    return 0;
}

int CDBAcounts::registered_del_one_account_node( CMyClientNode* ptr )
{
    int size;

    size = m_accounts_registered.count(ptr->m_account);
    mmp_printf("4.<<registered_del_one_account_node>>[ptr=%p,m_account=%s,total=%d]\n",ptr,ptr->m_account.c_str(),size);
    if( size )
    {
		MULTIMAP_STR2PTR::iterator itl;
		itl = m_accounts_registered.find(ptr->m_account);
		for( int i = 0; i < size; i++ )
		{
			if( itl == m_accounts_registered.end() )
			{
				mmp_printf(">>>REGISTERED DEL ERROR!!!<<<\n"); 			
				break;
			}
            if( itl->second == ptr )
            {
            	ptr->m_evtcomm.stop();		// first del the event
                m_accounts_registered.erase(itl);
                break;
            }			
            itl++;
			mmp_printf(">>>REGISTERED DEL WHILE<<<\n"); 			
		}
    }

	mmp_printf("4.>>>REGISTERED DEL:TOTAL CONNNECTIONS[%d]<<<\n", (int)m_accounts_registered.size());	
	
    return size;
}

int CDBAcounts::registered_del_all_account_node()
{
	MULTIMAP_STR2PTR::iterator   it;
	it = m_accounts_registered.begin();
	while( it != m_accounts_registered.end() )
	{
        CMyClientNode* ptr = it->second;
		delete ptr;
        m_accounts_registered.erase(it++);
	}
    return 0;
}

#define BEATHEART_TIMEOUT		120		// second
int CDBAcounts::registered_timeout_process()
{
	string account;
	CMyClientNode* pnode;
	MULTIMAP_STR2PTR::iterator   it;
	it = m_accounts_registered.begin();
	while( it != m_accounts_registered.end() )  
	{ 
        account	= it->first;
        pnode   = (CMyClientNode*)it->second;

		if( pnode->state_keep_above(BEATHEART_TIMEOUT) )		// have beat heart reset
		{
			pnode->m_evtcomm.stop();	// first del the event
			tobekilled_add_one_account_node(pnode);
			m_accounts_registered.erase(it++);
			mmp_printf(">>>YOU HAVE NO RECEIVED BEATHEART AMONG %d,TOTAL CONNNECTIONS[%d]<<<\n", BEATHEART_TIMEOUT, (int)m_accounts_registered.size());
		}
		else
			it++;
	}
	//print_all_conn_info();
	
    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int CDBAcounts::get_conn_ptr_from_one_account( string account, CMyClientNode** ppconn, int max )
{
    int size = m_accounts_registered.count(account);
	if( size > max ) size = max;
    if( size )
    {
		MULTIMAP_STR2PTR::iterator itl;
		itl = m_accounts_registered.find(account);
		for( int i = 0; i < size; i++ )
		{
			ppconn[i] = itl->second;
            itl++;
		}
    }
	return size;
}

int CDBAcounts::update_one_account_relation_info( CMyClientNode* pconn )
{
	int size;	

	pconn->m_active_relating_conn_cnt = 0;

	CMyClientNode* prelating_conn = NULL;
    string rele_account = pconn->m_related_account;
    size = m_accounts_registered.count(pconn->m_related_account);
    if( size )
    {
		MULTIMAP_STR2PTR::iterator itl;
		itl = m_accounts_registered.find(rele_account);
		for( int i = 0; i < size; i++ )
		{
			prelating_conn = itl->second;
			int ret = pconn->inc_active_related_conn_ptr(prelating_conn);
			if( ret == 1 )			// full
				break;
			itl++;
		}
    }
	mmp_printf("pconn[%p][%s] update size=%d\n",pconn,pconn->m_related_account.c_str(),size);
	return size;
}

int CDBAcounts::modify_one_account_relation_info( CMyClientNode* pconn )
{
	mmp_printf("[modify_one_account[%s]_relation_info][%s]-start\n",pconn->m_account.c_str(),pconn->m_related_account.c_str());	

	CMyClientNode* prelating_conn = NULL;
    string rele_account = pconn->m_related_account;
	// search all my releate accounts:
    int size = m_accounts_registered.count(rele_account);
    if( size )
    {
		MULTIMAP_STR2PTR::iterator itl;
		itl = m_accounts_registered.find(rele_account);
		for( int i = 0; i < size; i++ )
		{
			// get relating conn
			prelating_conn = itl->second;
			// check the relating conn's relating conn is myself or not
			if( prelating_conn && prelating_conn->m_related_account == pconn->m_account )
			{
				// 1 increase my active releate tab
				if( pconn->inc_active_related_conn_ptr(prelating_conn) == 1 )  // full
					break;
				// 2 increase my releating conn's active releate tab
				prelating_conn->inc_active_related_conn_ptr(pconn);
			}
            itl++;
			mmp_printf(">>>MODIFY WHILE<<<\n"); 
		}
    }
	
	mmp_printf("[modify_one_account[%s]_relation_info][%s]-over\n",pconn->m_account.c_str(),pconn->m_related_account.c_str());	

	return 0;
}

int CDBAcounts::release_one_account_relation_info(CMyClientNode *pconn, bool lock_en)
{
	int i;
	CMyClientNode* prelating_conn;

	//mmp_printf("[release_one_accounrelation_info]-[%s]-start\n",pconn->m_account.c_str());	

	// release all my relating conn's relating info about me
	for( i = 0; i < pconn->get_active_related_conn_cnt(); i++ )
	{
		prelating_conn = pconn->get_active_relating_conn_ptr(i);
		prelating_conn->dec_active_related_conn_ptr(pconn);
		mmp_printf("my relating conn[%d][%s][%p]release me...\n",i,prelating_conn->m_account.c_str(), prelating_conn );
	}
	
	// reset active releate conns tab
	pconn->reset_active_related_conn_cnt();

	//mmp_printf("[release_one_accounrelation_info]-[%s]-over\n",pconn->m_account.c_str());	

	return 0;
}

void CDBAcounts::print_all_conn_info()
{
	//pthread_mutex_lock(&m_registered_lock);

	mmp_printf("\n");
    MULTIMAP_STR2PTR::iterator it = m_accounts_registered.begin();
    int i = 0;
	CMyClientNode* pconn = NULL;
	CMyClientNode* prelating_conn = NULL;
    while( it != m_accounts_registered.end() )
	{
		pconn = it->second;
		mmp_printf("[%d][%s]:\n",i,pconn->m_account.c_str()); 	
		for( int k = 0; k < pconn->get_active_related_conn_cnt(); k++ )
		{
			prelating_conn = pconn->get_active_relating_conn_ptr(k);
			mmp_printf("    [A][%d][%s][%p]\n",k,prelating_conn->m_account.c_str(),prelating_conn);
		}
		it++;
        i++;
	}
	mmp_printf("\n");

	//pthread_mutex_unlock(&m_registered_lock);
}
