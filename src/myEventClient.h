#ifndef  _MYEVENTCLIENT_H_
#define _MYEVENTCLIENT_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <event.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/event.h>
#include <event2/thread.h>

#include <syslog.h>     
#define	mmp_printf2(fmt,...)	syslog(LOG_INFO, ""fmt"",##__VA_ARGS__)

class CMyEventClient
{
private:
    struct event_base 	*base;
	struct bufferevent 	*bev;

	static void ReadCb(struct bufferevent *bev, void *data);
	static void WriteCb(struct bufferevent *bev, void *data); 
	static void EventCb(struct bufferevent *bev, short events, void *data);

protected:

public:
    CMyEventClient();
    ~CMyEventClient();

	//
	int	 confd;
	void start( event_base *base, int fd, void *owner );
	void stop();

	//获取可读数据的长度
	int GetReadBufferLen(void)  
	{
		if( confd )			
			return evbuffer_get_length(bufferevent_get_input(bev)); 
		else
			return 0;
	}

	//从读缓冲区中取出len个字节的数据，存入buffer中，若不够，则读出所有数据
	//返回读出数据的字节数
	int GetReadBuffer(char *buffer, int len) 
	{
		if( confd )
			return evbuffer_remove(bufferevent_get_input(bev), buffer, len); 
		else
			return 0;
	}

	//将数据加入写缓冲区，准备发送
	//int SendWriteBuffer(char *buffer, int len) { return evbuffer_add(bufferevent_get_output(bev), buffer, len); }
	int SendWriteBuffer(char *buffer, int len) 	
	{
		if( confd )
		{
			mmp_printf2("SendWriteBuffer,buffer[0]=0x%02x,len=%d\n",buffer[0],len);
			return bufferevent_write(bev, buffer, len); 
		}
		else
			return 0;
	}
	
};

#endif
