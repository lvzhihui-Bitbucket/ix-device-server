
#ifndef _THREAD_POOL_H_
#define _THREAD_POOL_H_

#include <pthread.h>  
#include <stdio.h>  
#include <semaphore.h> 
#include <list>  
#include <exception>  
#include <errno.h>  
#include <iostream>  

class sem_locker  
{  
private:  
    sem_t m_sem;  
  
public:  
    sem_locker()  
    {  
        if(sem_init(&m_sem, 0, 0) != 0)  
            printf("sem init error\n");  
    }  
    ~sem_locker()  
    {  
        sem_destroy(&m_sem);  
    }  
  
    bool wait()  
    {  
        return sem_wait(&m_sem) == 0;  
    }  

    bool timedwait(int ms)
    {
        unsigned long nsec;
        struct timespec	wait_time;
		clock_gettime(CLOCK_REALTIME, &wait_time);
		nsec = wait_time.tv_nsec + ms*1000000;
		wait_time.tv_nsec = nsec%1000000000;
		wait_time.tv_sec += nsec/1000000000;
        return sem_timedwait(&m_sem,&wait_time) == 0;
    }

    bool add()  
    {  
        return sem_post(&m_sem) == 0;  
    }  
};  
  
  

class mutex_locker  
{  
private:  
    pthread_mutex_t m_mutex;  
  
public:  
    mutex_locker()  
    {  
        if(pthread_mutex_init(&m_mutex, NULL) != 0)  
            printf("mutex init error!");  
    }  
    ~mutex_locker()  
    {  
        pthread_mutex_destroy(&m_mutex);  
    }  
  
    bool mutex_lock()  //lock mutex  
    {  
        return pthread_mutex_lock(&m_mutex) == 0;  
    }  
    bool mutex_unlock()   //unlock  
    {  
        return pthread_mutex_unlock(&m_mutex) == 0;  
    }  
};  
  
class cond_locker  
{  
private:  
    pthread_mutex_t m_mutex;  
    pthread_cond_t m_cond;  
  
public:  
    cond_locker()  
    {  
        if(pthread_mutex_init(&m_mutex, NULL) != 0)  
            printf("mutex init error");  
        if(pthread_cond_init(&m_cond, NULL) != 0)  
        {   
            pthread_mutex_destroy(&m_mutex);  
            printf("cond init error");  
        }  
    }  
    // destroy mutex and cond  
    ~cond_locker()  
    {  
        pthread_mutex_destroy(&m_mutex);  
        pthread_cond_destroy(&m_cond);  
    }  
    bool wait()  
    {  
        int ans = 0;  
        pthread_mutex_lock(&m_mutex);  
        ans = pthread_cond_wait(&m_cond, &m_mutex);  
        pthread_mutex_unlock(&m_mutex);  
        return ans == 0;  
    }  
    bool signal()  
    {  
        return pthread_cond_signal(&m_cond) == 0;  
    }  
  
};  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>  
class threadpool  
{  
private:  
    int    			max_thread_num; 
    pthread_t       *thread_pids;   
    std::list<T *>  task_queue; 
    mutex_locker    queue_mutex_locker;
    sem_locker      task_run_queue_sem;
    bool        is_stop;
public:  
    threadpool(int thread_num = 20);  
    ~threadpool();  
    bool append_task(T *task);
    void start();  
    void stop();  
private:  
    static void *worker(void *arg); 
    void run();  
};
  
template <class T>  
threadpool<T>::threadpool(int thread_num): max_thread_num(thread_num)
{  
	is_stop = false;
	
    if( (thread_num <= 0) )  
        printf("threadpool can't init because max_thread_num = 0"  " or max_task_limit = 0");

    thread_pids = new pthread_t[max_thread_num];

    if(!thread_pids)  
        printf("can't init threadpool because thread array can't new");  
}  
  
template <class T>  
threadpool<T>::~threadpool()  
{  
    printf("exit threadpool!!\n");
    delete []thread_pids;  
    is_stop = true;  
}  
  
template <class T>  
void threadpool<T>::stop()  
{  
    is_stop = true;  
    for( int i = 0; i < max_thread_num; ++i )  
    {  
        task_run_queue_sem.add();  
    }
}  
  
template <class T>  
void threadpool<T>::start()  
{  
    for( int i = 0; i < max_thread_num; ++i )  
    {  
        printf("create the %dth pthread\n", i);  
        if( pthread_create( thread_pids + i, NULL, worker, this ) != 0 )  
        {
            delete []thread_pids;  
            throw std::exception();  
        }

        if( pthread_detach(thread_pids[i]) )  
        {
            delete []thread_pids;  
            throw std::exception();  
        }  
    }  
}

template <class T>  
bool threadpool<T>::append_task( T *task )  
{   
    queue_mutex_locker.mutex_lock();
    task_queue.push_back(task);  
    queue_mutex_locker.mutex_unlock();
    task_run_queue_sem.add(); 
    return true;  
}  
  
template <class T>  
void *threadpool<T>::worker(void *arg)  
{  
    threadpool *pool = (threadpool *)arg;  
    pool->run();  
    return pool;  
}  
  
template <class T>  
void threadpool<T>::run()  
{  
    while( !is_stop )  
    {   
        task_run_queue_sem.wait();      
        queue_mutex_locker.mutex_lock();  
        if(task_queue.empty())  
        {  
            queue_mutex_locker.mutex_unlock();  
            continue;  
        }
        T *task = task_queue.front();  
        task_queue.pop_front();  
        queue_mutex_locker.mutex_unlock();  
        if(!task)  continue;  
        task->doit();  //doit is T class's function
    }  
    printf("close %ld\n", (unsigned long)pthread_self());  
}  

#endif
