
#include "dbqueue.h"

CDataQueue::CDataQueue()
{
    pthread_mutex_init( &m_lock,0);
	m_dat_queue.empty();
	m_ptr_dat_queue.empty();
}

CDataQueue::~CDataQueue()
{
	clear_the_queue();
	clear_the_ptr_queue();
	pthread_mutex_destroy(&m_lock);
}

int CDataQueue::clear_the_queue()
{
	pthread_mutex_lock(&m_lock);

	DAT_BUFF* pnode;
	while( !m_dat_queue.empty() )
	{
		pnode = m_dat_queue.front();
		if( pnode != NULL )
		{
			if( pnode->pdat != NULL )
			{
				delete []pnode->pdat;
				pnode->pdat = NULL;
			}
			delete pnode;
			pnode = NULL;
		}			
		m_dat_queue.pop();
	}

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::copy_to_queue(CDataQueue* ptrQueue)
{
	pthread_mutex_lock(&m_lock);

    queue<DAT_BUFF*>* ptr_target = &ptrQueue->m_dat_queue;        
	// copy from source to target
	DAT_BUFF* pnode_s;
	DAT_BUFF* pnode_t;
	int size = m_dat_queue.size();
	for( int i = 0; i < size; i++ )
	{
		pnode_s = m_dat_queue.front();
		// new node, copy data
		pnode_t 		= new DAT_BUFF;
		pnode_t->len	= pnode_s->len;
		pnode_t->pdat 	= new char[pnode_s->len];
		memcpy( pnode_t->pdat, pnode_s->pdat, pnode_s->len);
		ptr_target->push(pnode_t);
		// notice: queue not support BIAN-LI,then pop followed push
		m_dat_queue.pop();
		m_dat_queue.push(pnode_s);
	}

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::push_into_queue(char* pdat,int len)
{
	pthread_mutex_lock(&m_lock);

    // new node, copy data
    DAT_BUFF* pnode = new DAT_BUFF;
    pnode->len	= len;
    pnode->pdat = new char[len];
    memcpy( pnode->pdat, pdat, len);
    // push into the queue
    m_dat_queue.push(pnode);

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::pop_from_queue(char* pdat, int* plen)
{
	pthread_mutex_lock(&m_lock);

	int size = m_dat_queue.size();
	if( size == 0 )
	{
		pthread_mutex_unlock(&m_lock);
		return 0;
	}

	DAT_BUFF* pnode = m_dat_queue.front();
	*plen = pnode->len;
	memcpy( pdat, pnode->pdat, pnode->len );
	delete []pnode->pdat;
	pnode->pdat = NULL;
	delete pnode;
	pnode = NULL;
	m_dat_queue.pop();		

	pthread_mutex_unlock(&m_lock);

	return size;
}
///////////////////////////////////////////////////////////////////////////
int CDataQueue::clear_the_ptr_queue()
{
	pthread_mutex_lock(&m_lock);

	PTR_DAT_BUFF* pnode;
	while( !m_ptr_dat_queue.empty() )
	{
		pnode = m_ptr_dat_queue.front();
		if( pnode != NULL )
		{
			if( pnode->pdat != NULL )
			{
				delete []pnode->pdat;
				pnode->pdat = NULL;
			}
			delete pnode;
			pnode = NULL;
		}			
		m_ptr_dat_queue.pop();
	}

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::copy_to_ptr_queue(CDataQueue* ptrQueue)
{
	pthread_mutex_lock(&m_lock);

    queue<PTR_DAT_BUFF*>* ptr_target = &ptrQueue->m_ptr_dat_queue;        
	// copy from source to target
	PTR_DAT_BUFF* pnode_s;
	PTR_DAT_BUFF* pnode_t;
	int size = m_ptr_dat_queue.size();
	for( int i = 0; i < size; i++ )
	{
		pnode_s = m_ptr_dat_queue.front();
		// new node, copy data
		pnode_t 		= new PTR_DAT_BUFF;
		pnode_t->ptr	= pnode_s->ptr;
		pnode_t->len	= pnode_s->len;
		pnode_t->pdat 	= new char[pnode_s->len];
		memcpy( pnode_t->pdat, pnode_s->pdat, pnode_s->len);
		ptr_target->push(pnode_t);
		// notice: queue not support BIAN-LI,then pop followed push
		m_ptr_dat_queue.pop();
		m_ptr_dat_queue.push(pnode_s);
	}

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::push_into_ptr_queue(void* ptr, char* pdat,int len)
{
	pthread_mutex_lock(&m_lock);

    // new node, copy data
    PTR_DAT_BUFF* pnode = new PTR_DAT_BUFF;
	pnode->ptr	= ptr;
    pnode->len	= len;
    pnode->pdat = new char[len];
    memcpy( pnode->pdat, pdat, len);
    // push into the queue
    m_ptr_dat_queue.push(pnode);

	pthread_mutex_unlock(&m_lock);

	return 0;
}

int CDataQueue::pop_from_ptr_queue(void** pptr, char* pdat, int* plen)
{
	pthread_mutex_lock(&m_lock);

	int size = m_ptr_dat_queue.size();
	if( size == 0 )
	{
		pthread_mutex_unlock(&m_lock);
		return 0;
	}

	PTR_DAT_BUFF* pnode = m_ptr_dat_queue.front();
	*pptr = pnode->ptr;
	*plen = pnode->len;
	memcpy( pdat, pnode->pdat, pnode->len );
	delete []pnode->pdat;
	pnode->pdat = NULL;
	delete pnode;
	pnode = NULL;
	m_ptr_dat_queue.pop();		

	pthread_mutex_unlock(&m_lock);

	return size;
}
