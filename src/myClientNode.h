
#ifndef _MY_CLIENT_NODE_H_
#define _MY_CLIENT_NODE_H_

#include "main.h"

#pragma pack(push)
#pragma pack(2)

#define MEDIA_TRANS_BUFF_MAX		1400
#define JPG_TRANS_BUFF_MAX			(MEDIA_TRANS_BUFF_MAX-8)
#define JPG_TRANS_BUFF_HEAD_LENTH	8

struct MEDIA_TYPE0_SEND_STRU
{
	short send_rand;
	int   total_len;
	short total_pack;
	short pack_id;
	short pack_lenth;
	char pack_dat[JPG_TRANS_BUFF_MAX];
};

#pragma pack(pop)

class CMyClientNode
{
    
public:
	struct event_base	*evbase;
	int 				client_fd;
	CMyEventClient		m_evtcomm;

	static const int 	STATE_UNREGISTER = 0;
	static const int 	STATE_REGISTERED = 1;
	static const int 	STATE_TOBEKILLED = 2;

	int					m_state;
	int 				m_state_delay;
	void*				m_owner;

	string 			    m_account;
	string			    m_password;
	string			    m_related_account;
	string				m_related_password;
	int					m_dev_type;
	int					m_ipv6;
	// 20210323_s
	int 				m_newrtp_type;	// m_dev_type:0/oldtype,1/newrtp, used for storing im's newrtp type
	int 				m_newrtp_send;	// m_dev_type:0/no send,1/have sent, used for storing app's just got newrtp type
	char*				m_newrtp_cmd_buff;
	int					m_newrtp_cmd_len;
	int set_newrtp_cmd_buff(char* buf, int len)
	{
		if( m_newrtp_cmd_buff != NULL ) delete m_newrtp_cmd_buff;
		m_newrtp_cmd_len 	= len;
		m_newrtp_cmd_buff	= new char[len];
		memcpy( m_newrtp_cmd_buff, buf, len);
		return m_newrtp_cmd_len;
	}
	char* get_newrtp_cmd_buff(int* plen)
	{
		*plen = m_newrtp_cmd_len;
		return m_newrtp_cmd_buff;
	}
	int set_newrtp_send(int send)
	{
		m_newrtp_send	= send;
		return 0;
	}
	int get_newrtp_send()
	{
		return m_newrtp_send;
	}
	// 20210323_e		
	/*---------------------------------------------------------------------------------------------------*/
	// node's account relationship
	pthread_mutex_t 	m_relation_lock;									
	static const int 	max_conns_of_account_to_relate = 10;			
	CMyClientNode		*m_active_relating_conns[max_conns_of_account_to_relate];
	int					m_active_relating_conn_cnt;
	static const int 	max_conns_of_account_be_related = 10;					
	void relate_lock();
	void relate_unlock();
	void reset_active_related_conn_cnt() { m_active_relating_conn_cnt = 0; }
	int inc_active_related_conn_ptr(CMyClientNode* pconn);
	int dec_active_related_conn_ptr(CMyClientNode* pconn);
	int get_active_related_conn_cnt();
	CMyClientNode* get_active_relating_conn_ptr(int cnt);
	/*---------------------------------------------------------------------------------------------------*/
	// node's media buffers for jpeg
	// type0 write pp buffer
	pthread_mutex_t 	m_rdbuf_lock;	
	bool				m_type0_vec_w_ind;	  // 0: write vector0�?1: write vector 1
	queue<DAT_BUFF*>   	m_type0_vec0;
	queue<DAT_BUFF*>   	m_type0_vec1;
	// type0 send buffer
	queue<DAT_BUFF*>   	m_type0_send_vec;

	queue<DAT_BUFF*>* get_type0_write_vec_ptr();
	queue<DAT_BUFF*>* get_type0_read_vec_ptr();
	void switch_type0_rd_vec_ptr();
	int clear_one_queue(queue<DAT_BUFF*>* ptr);		  
	int copy_my_queue(queue<DAT_BUFF*>* ptr_target);


	int write_local_buff(int media_type, char* pdat, int len);
	int clear_local_buff(int media_type);

	int is_send_buff_empty(void);	

	int update_send_buff(CMyClientNode* ptr_dat_source,int media_type);

	int clear_send_buff(void);	

	int pop_send_buff(char* pdat, int* plen);
	/*---------------------------------------------------------------------------------------------------*/
	CMyClientNode(event_base *base, int fd, int is_ipv6, string def_account, void* owner)
    {
		m_owner 		= owner;
		m_state			= STATE_UNREGISTER;
		m_state_delay	= 0;
		m_account		= def_account;

		m_active_relating_conn_cnt = 0;

		m_ipv6 				= is_ipv6;
		// 20210323_s
		m_newrtp_type		= 0;
		m_newrtp_send		= 0;
		m_newrtp_cmd_len	= 0;		
		m_newrtp_cmd_buff	= NULL;
		// 20210323_e		
		
    	pthread_mutex_init( &m_relation_lock,0);
    	pthread_mutex_init( &m_rdbuf_lock,0);

		//m_evtcomm.start(base,fd,this);
		evbase			= base;
		client_fd		= fd;

		store_zone = NULL;
		store_len = 0;
    }

	~CMyClientNode() 
    {
		m_state						= 0;
		m_active_relating_conn_cnt 	= 0;

		// 20210323_s
		if( m_newrtp_cmd_buff != NULL ) { delete m_newrtp_cmd_buff; m_newrtp_cmd_buff = NULL; }
		// 20210323_e		
		
        m_evtcomm.stop(); 

		//pthread_mutex_lock(&m_rdbuf_lock);		
		clear_one_queue(&m_type0_vec0);
		clear_one_queue(&m_type0_vec1);
		clear_one_queue(&m_type0_send_vec);	
		//pthread_mutex_unlock(&m_rdbuf_lock);		

		pthread_mutex_destroy(&m_relation_lock);
		pthread_mutex_destroy(&m_rdbuf_lock);
    }

	void commu_start()	{ m_evtcomm.start(	evbase,client_fd,this);	};
	void set_conn_state_UNREGISTER() 	{ m_state = STATE_UNREGISTER; m_state_delay = 0; }
	void set_conn_state_REGISTERED() 	{ m_state = STATE_REGISTERED; m_state_delay = 0; }
	void set_conn_state_TOBEKILLED() 	{ m_state = STATE_TOBEKILLED; m_state_delay = 0; }

	bool is_conn_state_UNREGISTER() 	{ return m_state == STATE_UNREGISTER; }
	bool is_conn_state_REGISTERED() 	{ return m_state == STATE_REGISTERED; }
	bool is_conn_state_TOBEKILLED() 	{ return m_state == STATE_TOBEKILLED; }

	void reset_conn_timeout_count() 	{ m_state_delay = 0; }

	void set_device_type(int type) 		{ m_dev_type = type; }
	void set_account(string account) 	{ m_account = account; }
	void set_passord( string password ) { m_password = password; }
	void set_related_account( string related_account ) { m_related_account = related_account; }
	void set_related_password( string related_password ) { m_related_password = related_password; }

	int state_keep_above( int max_delay ) { if( ++m_state_delay >= max_delay ) return 1; else return 0; }

	int server_notify_device_nat_ok(void);

	char* store_zone;
	int	store_len;
	int store_received_data(char* pbuf, int len);
	void* probe_received_data(int *plen);
	int dump_received_data(char* pbuf, int len);
};


#endif
