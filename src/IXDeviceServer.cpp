#include "main.h"
#include <algorithm>
#include <event2/thread.h>

#include "mySqlConnect.h"

#include "myClientNode.h"
#include "IXDeviceServer.h"
#include "dbaccounts.h"

void CIXDeviceServer::quit_cb(int sig, short events, void *arg)
{ 
	//mmp_printf("Catch the SIGINT signal, quit in one second\n");
	CIXDeviceServer *me = (CIXDeviceServer*)arg;
	timeval tv = {1, 0};
	me->stop_run(&tv);
}

void CIXDeviceServer::timeout_cb(int id, short events, void *arg)
{
	//mmp_printf("mediaclient: one second timing...!\n");
	COMMU_PACK_HEAD inner_msg;
	inner_msg.cmd_type 	= INNER_MSG_CMD_TYPE;
	inner_msg.dev_type	= INNER_MSG_TIMING_1S;
	CIXDeviceServer* pserver = (CIXDeviceServer*)arg;
	pserver->push_inner_msg_rcv_queue((void*)pserver,(char*)&inner_msg, sizeof(inner_msg),NULL,0);	
}

void *CIXDeviceServer::sub_thread_libevent(void *arg)
{
	LibeventThread *me = (LibeventThread*)arg;
	event_base_dispatch(me->base);
	return (void*)me;
}

bool CIXDeviceServer::start_run()
{
	//listenning to fixed port
	evconnlistener *listener=NULL;

	struct sockaddr_storage sin;
	memset(&sin, 0, sizeof(sin));
	(*(struct sockaddr_in6*)&sin).sin6_family = AF_INET6; 
    (*(struct sockaddr_in6*)&sin).sin6_addr  = in6addr_any;
    (*(struct sockaddr_in6*)&sin).sin6_port = htons(m_port);	
	listener = evconnlistener_new_bind(m_MainThread->base, listenner_event_cb, (void*)this,LEV_OPT_REUSEABLE|LEV_OPT_CLOSE_ON_FREE, -1,(sockaddr*)&sin, sizeof(sin));

	if( NULL == listener ) mmp_printf("TCP listen error");

	//start sub threads
	for( int i = 0; i  <m_threads_cnt; i++ )
	{
		pthread_create(&m_SubThreads[i].tid, NULL, sub_thread_libevent, (void*)&m_SubThreads[i]);
	}

	// create account messge process task
	start_inner_msg_rcv_queue_thread();

	// ctrl + c
	ctrlc_ev = evsignal_new(m_MainThread->base, SIGINT, quit_cb, (void*)this);
	if ( !ctrlc_ev || event_add(ctrlc_ev, NULL) < 0 )
	{
		event_del(ctrlc_ev);
	}
	// 1s timerout
	timeout_ev = new event;
	event_assign(timeout_ev, m_MainThread->base, -1, EV_PERSIST, timeout_cb, (void*)this);
	timeval tv = {1,0};
	if( event_add(timeout_ev, &tv) < 0 )
	{
		event_del(timeout_ev);
	}		
	// enter main event loop
	event_base_dispatch(m_MainThread->base);

	// exit main event loop
	mmp_printf("StartRun Over!!!\n");

	if( m_port != EXIT_CODE )
	{
		evconnlistener_free(listener);
	}
	
	return true;
}

void CIXDeviceServer::stop_run(timeval *tv)
{
	int contant = EXIT_CODE;
	for( int i = 0; i < m_threads_cnt; i++ )
	{
		int size = write(m_SubThreads[i].notifySendFd, &contant, sizeof(int));
		mmp_printf("stop_run write bytes[%d]\n",size);
	}
	event_base_loopexit(m_MainThread->base, tv);

	// stop account messge process task
	stop_inner_msg_rcv_queue_thread();	
}

void CIXDeviceServer::listenner_event_cb( struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *sa, int socklen, void *user_data )
{
	CIXDeviceServer *server = (CIXDeviceServer*)user_data;
	int num = rand() % server->m_threads_cnt;
	int sendfd = server->m_SubThreads[num].notifySendFd;
		
	//int size = write(sendfd, &fd, sizeof(evutil_socket_t));
	int fd_tmp = fd;
	if( sa->sa_family == AF_INET6 ) 
	{
		char buf[NI_MAXHOST];
		struct sockaddr_in6 *add6;
		add6 = (struct sockaddr_in6 *)sa;
		inet_ntop(sa->sa_family, (void *)&add6->sin6_addr, buf, NI_MAXHOST);
		mmp_printf("sa_ipv6[%s]\n",buf);
		if( strstr(buf,"::ffff:") == NULL )
		{
			fd_tmp |= 0x80000000;
			mmp_printf("sa_family: AF_INET6\n");
		}
		else
			mmp_printf("sa_family: AF_INET4\n");
	}
	else
		mmp_printf("sa_family: AF_INET4\n");
		
	int size = write(sendfd, &fd_tmp, sizeof(int));
	
	size = size;
	//mmp_printf("listenner_event_cb write bytes[%d]\n",size);
}

void CIXDeviceServer::one_connect_process( int fd, short which, void *arg )
{
	LibeventThread *me = (LibeventThread*)arg;

	int pipefd = me->notifyReceiveFd;
	evutil_socket_t confd;
	//int size = read(pipefd, &confd, sizeof(evutil_socket_t));
	int confd_tmp;
	int size = read(pipefd, &confd_tmp, sizeof(int));
		
	size = size;
	//mmp_printf("one_connect_process read bytes[%d]\n",size);
	// if exit code, then close the thread
	if( EXIT_CODE == confd_tmp )
	{
		mmp_printf("EXIT_CODE");
		event_base_loopbreak(me->base);
		return;
	}

	int is_ipv6;
	if( confd_tmp&0x80000000 ) 
		is_ipv6 = 1;
	else
		is_ipv6 = 0;
	confd = confd_tmp&(~0x80000000);
	
	// get one connect, create node info and add to list
	CMyClientNode* pnode = new CMyClientNode(me->base,confd,is_ipv6,UNREGISTER_ACCOUNT,me->pserver);
	me->pserver->m_all_node.unregister_add_one_account_node( pnode );
	pnode->commu_start();
	mmp_printf("one new client node start...");
}

void CIXDeviceServer::setup_thread(LibeventThread *me)
{
	evthread_use_pthreads();

	me->pserver = this;
	me->base 	= event_base_new();
	if( NULL == me->base )
		mmp_printf("event base new error");

	evthread_make_base_notifiable(me->base);

	int fds[2];
	int nowarm = pipe(fds); nowarm = nowarm;
	me->notifyReceiveFd = fds[0];
	me->notifySendFd = fds[1];

	event_set( &me->notifyEvent, me->notifyReceiveFd, EV_READ | EV_PERSIST, one_connect_process, me );
	event_base_set(me->base, &me->notifyEvent);
	if( event_add(&me->notifyEvent, 0) == -1 )
	{
		mmp_printf("Can't monitor libevent notify pipe\n");
	}
}

CIXDeviceServer::CIXDeviceServer(int count) 
{
	evthread_use_pthreads();

	queue_thread_en 	= 0;

	m_threads_cnt 		= count;
	m_MainThread 		= new LibeventThread;
	m_SubThreads 		= new LibeventThread[m_threads_cnt];
	m_MainThread->tid 	= pthread_self();
	m_MainThread->base 	= event_base_new();
	evthread_make_base_notifiable(m_MainThread->base);

	for( int i = 0; i < m_threads_cnt; i++ )
	{
		setup_thread(&m_SubThreads[i]);
	}
}   

CIXDeviceServer::~CIXDeviceServer() 
{
	stop_run(NULL);

	event_del(ctrlc_ev);
	event_del(timeout_ev);	
	event_base_free(m_MainThread->base);
	for( int i = 0; i < m_threads_cnt; i++ )
		event_base_free(m_SubThreads[i].base);

	delete m_MainThread;
	delete []m_SubThreads;

    mmp_printf("<<CIXDeviceServer::~CIXDeviceServer>>\n");
}

void CIXDeviceServer::RcvBufProcess(CMyClientNode* conn, char* rd_buf, int len)
{
	if( conn == NULL || rd_buf == NULL )
		return;
	
	COMMU_PACK_HEAD* ptr_head 	= (COMMU_PACK_HEAD*)rd_buf;
	CIXDeviceServer* pserver 	= (CIXDeviceServer*)conn->m_owner;

	if( ptr_head->cmd_type == INNER_MSG_CMD_TYPE )
	{
		if( ptr_head->dev_type == INNER_MSG_CLIENT_CLOSED )
			mmp_printf("connect closed reason[CLIENT_CLOSED]\n");
		else if( ptr_head->dev_type == INNER_MSG_CLIENT_ERROR )
			mmp_printf("connect closed reason[CLIENT_ERROR]\n");
		else if( ptr_head->dev_type == INNER_MSG_CLIENT_ERROR_OTHERS )
			mmp_printf("connect closed reason[CLIENT_ERROR_OTHERS]\n");	
		else
			mmp_printf("connect closed reason[UNKOWN]\n");	
			
		if( conn->is_conn_state_REGISTERED() )
		{
			pserver->m_all_node.release_one_account_relation_info(conn,true);
			pserver->m_all_node.registered_del_one_account_node(conn);		// move registered map to tobekilled map
			pserver->m_all_node.tobekilled_add_one_account_node(conn);		// add to tobekilled map for be killed
		}
		else if( conn->is_conn_state_UNREGISTER() )
		{
			pserver->m_all_node.unregister_del_one_account_node(conn,true);	// kill the unregister node
		}
		else
		{
			pserver->m_all_node.tobekilled_del_one_account_node(conn);
		}
	}
	else if( conn->is_conn_state_UNREGISTER() )
	{
		if( ptr_head->cmd_type == NORMAL_LOGIN_REQ )
		{
			COMMU_PACK_NORMAL_LOGIN_REQ *ptr_account_req = (COMMU_PACK_NORMAL_LOGIN_REQ*)rd_buf;
			COMMU_PACK_NORMAL_LOGIN_RSP 	account_rsp;
			string account 			= ptr_account_req->userid;
			string password 		= ptr_account_req->password;

			if( ptr_account_req->head.dev_type == ACCOUNT_TYPE_APP && (account==TEST_USER_ID) && (password==TEST_USER_PSD) )
			{
				pserver->m_all_node.unregister_del_one_account_node(conn,false);

				conn->set_device_type(ptr_account_req->head.dev_type);
				conn->set_account(account);
				conn->set_passord(password);
				conn->set_related_account("");
				conn->set_related_password("");
				pserver->m_all_node.registered_add_one_account_node(conn);
				
				pserver->m_all_node.modify_one_account_relation_info(conn);
								
				account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
				account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
				account_rsp.login			= ptr_account_req->login;
				account_rsp.reserve			= ptr_account_req->reserve;
				memcpy(account_rsp.userid,ptr_account_req->userid,sizeof(ptr_account_req->userid));
				memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));			
				account_rsp.head.cmd_type 	= NORMAL_LOGIN_RSP;
				account_rsp.result 			= 0;	// ok
				conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_LOGIN_RSP));			

				mmp_printf("NORMAL_LOGIN_REQ[userid=%s],[psw=%s],[datlen=%d]\n",account.c_str(),password.c_str(),ptr_account_req->head.dat_len);
			}
			else
			{
				account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
				account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
				account_rsp.login			= ptr_account_req->login;
				account_rsp.reserve			= ptr_account_req->reserve;
				memcpy(account_rsp.userid,ptr_account_req->userid,sizeof(ptr_account_req->userid));
				memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
				account_rsp.head.cmd_type 	= NORMAL_LOGIN_RSP;
				account_rsp.result 			= 1;	// err
				conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_LOGIN_RSP));	

				mmp_printf("NORMAL_LOGIN_REQ[userid=%s],[psw=%s],[datlen=%d],type error!\n",account.c_str(),password.c_str(),ptr_account_req->head.dat_len);
			}
		}

		else if( ptr_head->cmd_type == PROXY_ONLINE_REQ )
		{
			COMMU_PACK_NORMAL_ONLINE_REQ *ptr_account_req = (COMMU_PACK_NORMAL_ONLINE_REQ*)rd_buf;
			COMMU_PACK_NORMAL_ONLINE_RSP 	account_rsp;
			string account 			= ptr_account_req->account;
			string password 		= ptr_account_req->password;

			if( ptr_account_req->head.dev_type == ACCOUNT_TYPE_MONITOR ) // && check_account_password(account,password) )
			{
				pserver->m_all_node.unregister_del_one_account_node(conn,false);
				conn->set_device_type(ptr_account_req->head.dev_type);
				conn->set_account(account);
				conn->set_passord(password);
				conn->set_related_account("");
				conn->set_related_password("");
				pserver->m_all_node.registered_add_one_account_node(conn);

				pserver->m_all_node.modify_one_account_relation_info(conn);

				account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
				account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
				account_rsp.online			= ptr_account_req->online;
				account_rsp.reserve			= ptr_account_req->reserve;
				memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
				memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
				account_rsp.head.cmd_type 	= PROXY_ONLINE_RSP;
				account_rsp.result 			= 0;	// ok
				conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_ONLINE_RSP) );

				mmp_printf("PROXY_ONLINE_REQ[proxyid=%s],[psw=%s],[datlen=%d]\n",account.c_str(),password.c_str(),ptr_account_req->head.dat_len);
			}
			else
			{
				account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
				account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
				account_rsp.online			= ptr_account_req->online;
				account_rsp.reserve			= ptr_account_req->reserve;
				memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
				memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
				account_rsp.head.cmd_type 	= PROXY_ONLINE_RSP;
				account_rsp.result 			= 1;	// err
				conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_ONLINE_RSP) );

				mmp_printf("PROXY_ONLINE_REQ[proxyid=%s],[psw=%s],[datlen=%d],type error!\n",account.c_str(),password.c_str(),ptr_account_req->head.dat_len);
			}
		}				
	}
	
	else if( conn->is_conn_state_REGISTERED() )
	{
		if( ptr_head->cmd_type == NORMAL_BEAT_HEART_REQ )
		{	
			conn->reset_conn_timeout_count();	

			COMMU_PACK_NORMAL_BEAT_HEART_REQ *ptr_account_req = (COMMU_PACK_NORMAL_BEAT_HEART_REQ*)rd_buf;
			COMMU_PACK_NORMAL_BEAT_HEART_RSP 	account_rsp;
			account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
			account_rsp.head.dat_len	= ptr_account_req->head.dat_len;
			account_rsp.head.cmd_id 	= ptr_account_req->head.cmd_id;
			account_rsp.id				= ptr_account_req->id;
			account_rsp.reserve			= ptr_account_req->reserve;
			account_rsp.head.cmd_type 	= NORMAL_BEAT_HEART_RSP;
			conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BEAT_HEART_RSP) );

			mmp_printf("responsing normal beat heart...\n");
		}

		else if( ptr_head->cmd_type == PC_BIND_PROXY_REQ )
		{
			COMMU_PACK_NORMAL_BIND_REQ *ptr_account_req = (COMMU_PACK_NORMAL_BIND_REQ*)rd_buf;
			COMMU_PACK_NORMAL_BIND_RSP 	account_rsp;
			string account 			= ptr_account_req->account;
			string password 		= ptr_account_req->password;

			// check local addcout password and relation account password
			if( ptr_account_req->head.dev_type == ACCOUNT_TYPE_MONITOR )  //&& check_account_password(account,password) )
			{
				mmp_printf("PC_BIND_PROXY_REQ[ER]=%s,psw=%s,type=%d\n",account.c_str(),password.c_str(),ptr_account_req->head.dev_type);
				// not change node's status

				account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
				account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
				account_rsp.reserve			= ptr_account_req->reserve;
				memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
				memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
				account_rsp.head.cmd_type 	= PC_BIND_PROXY_RSP;
				account_rsp.result 			= 1;	// target proxy id account err
				conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BIND_RSP) );				
			}
			else
			{
				// unbind request  (the conn's related account equl the req account)
				if(  ptr_account_req->bind == 0 )
				{
					if( (conn->m_related_account == ptr_account_req->account) && (conn->m_related_password == ptr_account_req->password) )
					{
						// release myself relating info
						pserver->m_all_node.release_one_account_relation_info(conn,false);

						account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
						account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
						account_rsp.reserve			= ptr_account_req->reserve;
						memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
						memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
						account_rsp.head.cmd_type 	= PC_BIND_PROXY_RSP;
						account_rsp.result 			= 0;	// unbind ok
						conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BIND_RSP) );				
					}
					else
					{
						account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
						account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
						account_rsp.reserve			= ptr_account_req->reserve;
						memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
						memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
						account_rsp.head.cmd_type 	= PC_BIND_PROXY_RSP;
						account_rsp.result 			= 2;	// unbind proxy id just NOT match
						conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BIND_RSP) );				
					}
				}
				// bind request
				else
				{
					// get cur conn's related account's conn ptrs ( this is just proxy id)
					CMyClientNode* conn_ptrs[5];
					int max_bind_proxy = pserver->m_all_node.get_conn_ptr_from_one_account(account,conn_ptrs,5);

					mmp_printf("[%s->%s][%s]:%d\n",conn->m_account.c_str(),conn->m_related_account.c_str(),account.c_str(),max_bind_proxy);
					
					if( max_bind_proxy )
					{
						// 1. add related account and password - for the conn
						// 2. add related account and password - for the conn's related account
						// 3. modify_one_account_relation_info - for the conn
						// 4. modify_one_account_relation_info - for the conn's related account
						// cur conn's related account just NOT equal apply account
						if( conn->m_related_account != account )
						{
							// already related other account
							if( !conn->m_related_account.empty() )
							{
								// 1. release cur conn's relating info
								pserver->m_all_node.release_one_account_relation_info(conn,false);
								// 2. release cur conn's related conn's relating info
								for( int i = 0; i < max_bind_proxy; i++ )
								{
									pserver->m_all_node.release_one_account_relation_info(conn_ptrs[i],false);
								}
							}
						}
						// set new related account
						conn->set_related_account(account);
						conn->set_related_password(password);
						// modify cur conn's related info
						pserver->m_all_node.modify_one_account_relation_info(conn);

						for( int i = 0; i < max_bind_proxy; i++ )
						{
							// set new related account
							conn_ptrs[i]->set_related_account(conn->m_account);
							conn_ptrs[i]->set_related_password(conn->m_password);
							// modify cur conn's ralated conn's relating info
							pserver->m_all_node.modify_one_account_relation_info(conn_ptrs[i]);
						}

						mmp_printf("[%s->%s][%s]:%d\n",conn->m_account.c_str(),conn->m_related_account.c_str(),account.c_str(),max_bind_proxy);

						account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
						account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
						account_rsp.reserve			= ptr_account_req->reserve;
						memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
						memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
						account_rsp.head.cmd_type 	= PC_BIND_PROXY_RSP;
						account_rsp.result 			= 0;	// bind ok!
						conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BIND_RSP) );
					}
					else
					{
						account_rsp.head.dev_type	= ptr_account_req->head.dev_type;
						account_rsp.head.dat_len	= ptr_account_req->head.dat_len+2;
						account_rsp.reserve			= ptr_account_req->reserve;
						memcpy(account_rsp.account,ptr_account_req->account,sizeof(ptr_account_req->account));
						memcpy(account_rsp.password,ptr_account_req->password,sizeof(ptr_account_req->password));
						account_rsp.head.cmd_type 	= PC_BIND_PROXY_RSP;
						account_rsp.result 			= 3;	// bind target proxy id just NOT exist!
						conn->m_evtcomm.SendWriteBuffer( (char*)&account_rsp, sizeof(COMMU_PACK_NORMAL_BIND_RSP) );
					}
				}
			}
		}				

		else if( ptr_head->cmd_type == NORMAL_TRANSFER_DAT ) 
		{
			COMMU_PACK_NORMAL_TRANSFER_DAT *ptr_tran_req = (COMMU_PACK_NORMAL_TRANSFER_DAT*)rd_buf;
			int related_conn_cnt = conn->get_active_related_conn_cnt();
			if( related_conn_cnt == 0 )
			{
				mmp_printf("conn[%s] transfer data,have no related account!!!\n",conn->m_account.c_str());				
			}
			else
			{
				for( int i = 0; i < conn->get_active_related_conn_cnt(); i++ )
				{
					CMyClientNode *ptr_related = conn->get_active_relating_conn_ptr(i);
					if( ptr_related != NULL )
					{
						mmp_printf("conn[%s] transfer data to relate conn[%s]\n",conn->m_account.c_str(), ptr_related->m_account.c_str() );

						ptr_related->m_evtcomm.SendWriteBuffer(rd_buf,len);

						char tmpbuf[200];
						char tmpdat[4000];
						memset(tmpbuf,0,200);
						memset(tmpdat,0,4000);
						mmp_printf("len=%d, head=%d\n",len,sizeof(COMMU_PACK_HEAD));
						sprintf(tmpbuf,"NORMAL_TRANSFER_DAT transfer dat:");
						for( int k = 0; k < len-sizeof(COMMU_PACK_HEAD); k++ ) // exclude: 4 bytes head + 2 bytes code
						{
							sprintf(tmpdat+2*k,"%02x",ptr_tran_req->dat[k]);
						}
						mmp_printf("%s%s\n",tmpbuf,tmpdat);
					}
					else
					{
						mmp_printf("NORMAL_TRANSFER_DAT NO Binding!!!\n");
					}		
				}
			}
		}
	}	
}


// lzh_20210208_s
void CIXDeviceServer::start_inner_msg_rcv_queue_thread(void)
{
	if( !queue_thread_en )
	{
		queue_thread_en = 1;		
		pthread_mutexattr_init(&queue_thread_attr);
		pthread_mutexattr_settype(&queue_thread_attr,PTHREAD_MUTEX_RECURSIVE_NP); // set lock recursive
		pthread_mutex_init(&queue_thread_lock,&queue_thread_attr);	
		//pthread_mutex_init(&queue_thread_lock, NULL); 
		pthread_cond_init(&queue_thread_cond, NULL);

		pthread_create(&queue_thread_tid, NULL, inner_msg_dat_process, (void*)this);	

		mmp_printf("start_inner_msg_rcv_queue_thread...\n");
	}
}

void CIXDeviceServer::stop_inner_msg_rcv_queue_thread(void)
{
	if( queue_thread_en )
	{
		queue_thread_en = 0;	
		pthread_cancel(queue_thread_tid);
		pthread_join(queue_thread_tid,NULL);
		
		pthread_mutex_destroy(&queue_thread_lock);
		pthread_cond_destroy(&queue_thread_cond);

		mmp_printf("stop_inner_msg_rcv_queue_thread...\n");
	}
}

int CIXDeviceServer::push_inner_msg_rcv_queue( void *arg, char* pbuf, int len, CMyClientNode *owner, int ext_type )
{
	CIXDeviceServer *me = (CIXDeviceServer*)arg;
	int was_empty;

	if( !me->queue_thread_en )
		return 0;

	pthread_mutex_lock(&me->queue_thread_lock);	

	was_empty = me->queue_dat.empty();	
	
	// new node, copy data
	inner_msg_data* pnode = new inner_msg_data;
	pnode->rcv_len		= len;
	pnode->rcv_pbuf		= new char[len];
	memcpy( pnode->rcv_pbuf, pbuf, len);
	pnode->conn 		= owner;
	pnode->type			= ext_type;
	// push into the queue
	me->queue_dat.push(pnode);	

	if( was_empty )
	{
		//pthread_cond_broadcast(&queue_thread_cond);
		pthread_cond_signal(&me->queue_thread_cond);
		//mmp_printf("pthread_cond_signal\n");		
	}
	
	pthread_mutex_unlock(&me->queue_thread_lock);
	
	return 0;
}

inner_msg_data* CIXDeviceServer::pop_inner_msg_rcv_queue( void *arg )
{
	CIXDeviceServer *me = (CIXDeviceServer*)arg;

	pthread_mutex_lock(&me->queue_thread_lock);	

	if( me->queue_dat.empty() ) 
    { 
		//mmp_printf("pthread_cond_wait,fd[%d]\n",me->session_fd);		
        pthread_cond_wait(&me->queue_thread_cond, &me->queue_thread_lock);
    }
	
	inner_msg_data* pnode = me->queue_dat.front();
	me->queue_dat.pop();

	//mmp_printf("pthread_cond_wait over\n");		
	
	pthread_mutex_unlock(&me->queue_thread_lock);

	return pnode;
}

void CIXDeviceServer::clean_inner_msg_rcv_queue_thread(void * arg)
{
	CIXDeviceServer *me = (CIXDeviceServer*)arg;
	
	pthread_mutex_lock(&me->queue_thread_lock); 
	
	inner_msg_data* pnode;
	while( !me->queue_dat.empty() )
	{
		pnode = me->queue_dat.front();
		if( pnode != NULL )
		{
			if( pnode->rcv_pbuf != NULL )
			{
				delete []pnode->rcv_pbuf;
				pnode->rcv_pbuf = NULL;
			}
			delete pnode;
			pnode = NULL;
		}			
		me->queue_dat.pop();
	}	
	
	pthread_mutex_unlock(&me->queue_thread_lock);	

	mmp_printf("clean_inner_msg_rcv_queue_thread!!!\n");
	
}

void* CIXDeviceServer::inner_msg_dat_process(void *arg)
{
	CIXDeviceServer *me = (CIXDeviceServer*)arg;

	inner_msg_data* pnode;

	pthread_cleanup_push(clean_inner_msg_rcv_queue_thread,me);

	mmp_printf("inner_msg_dat_process\n");

	int one_second_cnt = 60;		
	while(1)
	{
		pnode = pop_inner_msg_rcv_queue(me);
		if( pnode != NULL )
		{
			if( !pnode->type )
			{
				// inner message process
				COMMU_PACK_HEAD* ptr_head = (COMMU_PACK_HEAD*)pnode->rcv_pbuf;
				if( ptr_head->head[0] == HEAD_FLAG_1 && ptr_head->head[1] == HEAD_FLAG_2 &&
					ptr_head->head[2] == HEAD_FLAG_3 && ptr_head->head[3] == HEAD_FLAG_4 )
				{
					if( (ptr_head->cmd_type == INNER_MSG_CMD_TYPE) && (ptr_head->dev_type == INNER_MSG_TIMING_1S) )
					{
						me->m_all_node.unregister_timeout_process();
						me->m_all_node.registered_timeout_process();
						me->m_all_node.tobekilled_timeout_process();
						if( ++one_second_cnt >= 60  )
						{
							one_second_cnt = 0;
							mmp_printf("one second timing processs\n");
						}
					}
					else
					{
						//mmp_printf("RcvBufProcess-Is INNER package\n");
						RcvBufProcess(pnode->conn, pnode->rcv_pbuf, pnode->rcv_len);
					}
				}
			}
			else
			{
				// Is NOT combine package
				COMMU_PACK_HEAD* ptr_head = (COMMU_PACK_HEAD*)pnode->rcv_pbuf;
				if( ptr_head->head[0] == HEAD_FLAG_1 && ptr_head->head[1] == HEAD_FLAG_2 &&
					ptr_head->head[2] == HEAD_FLAG_3 && ptr_head->head[3] == HEAD_FLAG_4 &&
					(ptr_head->dat_len + sizeof(COMMU_PACK_HEAD)) ==  pnode->rcv_len )
				{
						//mmp_printf("RcvBufProcess-Is SINGLE package\n");
						RcvBufProcess(pnode->conn, pnode->rcv_pbuf, pnode->rcv_len);
				}
				// Is ombine package
				else
				{
					mmp_printf("RcvBufProcess-Is MULTI package\n");

					// storing recevied data to conn's rec_buffer
					pnode->conn->store_received_data(pnode->rcv_pbuf,pnode->rcv_len);
					// parase conn's rec_buffer
					char* rcv_pbuf;
					int	rcv_len;
					while(1)
					{
						ptr_head = (COMMU_PACK_HEAD*)pnode->conn->probe_received_data(&rcv_len);
						// data package NOT receivied completed
						if( rcv_len < sizeof(COMMU_PACK_HEAD) )
						{
							if( rcv_len == 0 )
								mmp_printf("Stored data is empty!![%d]\n");
							else
								mmp_printf("Probe stored data length is too short[%d]\n",rcv_len);
							break;
						}
						mmp_printf("Probe stored data length is %d\n",rcv_len);

						// probe data package is ok
						if( ptr_head->head[0] == HEAD_FLAG_1 && ptr_head->head[1] == HEAD_FLAG_2 &&
							ptr_head->head[2] == HEAD_FLAG_3 && ptr_head->head[3] == HEAD_FLAG_4 )
						{
							// just available data package
							if( rcv_len >= (ptr_head->dat_len + sizeof(COMMU_PACK_HEAD)) )
							{
								// get data package length
								rcv_len	 = ptr_head->dat_len + sizeof(COMMU_PACK_HEAD);
								// get data buffer
								rcv_pbuf = new char[rcv_len];
								// dump data package
								pnode->conn->dump_received_data( rcv_pbuf, rcv_len );
								// data package process
								RcvBufProcess(pnode->conn, rcv_pbuf, rcv_len);
								// release buffer
								delete []rcv_pbuf;
								//mmp_printf("Dumped data length is %d\n",rcv_len);
							}
							// data package NOT received completed
							else
							{
								//mmp_printf("Remain data length is %d\n",rcv_len);
								break;
							}
						}
						// probe data package is ERR
						else
						{
							// get data buffer
							rcv_pbuf = new char[rcv_len];
							// dump data package and lose it
							pnode->conn->dump_received_data( rcv_pbuf, rcv_len );
							mmp_printf("ERR DATA[%02x-%02x-%02x-%02x]\n",rcv_pbuf[0],rcv_pbuf[1],rcv_pbuf[2],rcv_pbuf[3]);
							// release buffer
							delete []rcv_pbuf;
							break;
						}
					}
				}
			}
			// release node
			if( pnode->rcv_pbuf != NULL )
			{
				delete []pnode->rcv_pbuf;
				pnode->rcv_pbuf = NULL;
			}
			delete pnode;
			pnode = NULL;
		}
	}

	pthread_cleanup_pop(0);
	
	return 0;	
}


// lzh_20210208_e

