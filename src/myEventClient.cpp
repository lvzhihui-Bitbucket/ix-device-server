
#include "main.h"
#include "myClientNode.h"
#include "dbaccounts.h"
#include "IXDeviceServer.h"

CMyEventClient::CMyEventClient()
{
	base 	= NULL;
	bev 	= NULL;
	confd	= 0;
}

CMyEventClient::~CMyEventClient()
{	
	// when server remote close the connect, so have no socket to send data, if client be deleted to call this bufferevent_free, just error!
	if( bev != NULL )
	{
		bufferevent_disable(bev, EV_READ|EV_WRITE);
		bufferevent_free(bev);
		bev = NULL;
	}
	if( confd )
	{
		close(confd);
		confd = 0;
	}
}

void CMyEventClient::start(event_base *base, int fd, void *owner)
{
	if( bev == NULL )
	{
		/*
		BEV_OPT_CLOSE_ON_FREE：当bufferevent释放时，关闭底层传输
		BEV_OPT_THREADSAFE：为bufferevent使用lock
		BEV_OPT_DEFER_CALLBACKS：将callback设为延迟的
		BEV_OPT_UNLOCK_CALLBACKS：默认情况下如果有THREADSAFE标志，调用callback时会加锁。使用这个标志是的即便有THREADSAFE标志，调用callback也不加锁		
		*/
		bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE|BEV_OPT_THREADSAFE|BEV_OPT_DEFER_CALLBACKS);
		if( bev != NULL )
		{
			bufferevent_setcb(bev, ReadCb, WriteCb, EventCb, owner);
			bufferevent_enable(bev, EV_READ|EV_WRITE);
			confd = fd;
			mmp_printf("bufferevent_socket_new ok[%d]\n",confd);
		}
		else
		{
			mmp_printf("bufferevent_socket_new NULL\n");
		}
	}
}

void CMyEventClient::stop()
{
	if( bev != NULL )
	{
		mmp_printf("bufferevent_disable1\n");
		bufferevent_disable(bev, EV_READ|EV_WRITE);
		mmp_printf("bufferevent_disable2\n");
		bufferevent_free(bev);		// 释放bfferevent。如果callback是defered的，那么bufferevent会等到callback返回之后才释放
		bev = NULL;
		mmp_printf("bufferevent_disable3\n");
	}
	if( confd )
	{
		mmp_printf("close confd[%d] start\n",confd);
		close(confd);
		confd = 0;
		mmp_printf("close confd over\n");
	}
}

void CMyEventClient::ReadCb(struct bufferevent *bev, void *data)
{
	if( data == NULL )
	{
		mmp_printf("ReadCb ERROR!!!\n");
		return;
	}

	//mmp_printf("ReadCb:read callback...\n");
	
	CMyClientNode *owner = (CMyClientNode*)data;
	struct evbuffer *input = bufferevent_get_input(bev);
	int size = evbuffer_get_length(input);

	char 	rd_buf[20*1024];
	evbuffer_remove(input,rd_buf,size);
	CIXDeviceServer* pserver = (CIXDeviceServer*)owner->m_owner;
	pserver->push_inner_msg_rcv_queue((void*)pserver,rd_buf,size,owner,1);		
    
	//mmp_printf("ReadCb:read callback bytes[%d]\n",size);
}

void CMyEventClient::WriteCb(struct bufferevent *bev, void *data)
{
	//CMyClientNode *owner = (CMyClientNode*)data;
}


/*
BEV_EVENT_READING
BEV_EVENT_WRITING
BEV_EVENT_ERROR
BEV_EVENT_TIMEOUT
BEV_EVENT_EOF
BEV_EVENT_CONNECTED
*/
void CMyEventClient::EventCb(struct bufferevent *bev, short events, void *data)
{
	if( data == NULL )
	{
		mmp_printf("EventCb ERROR!!!\n");
		return;
	}

	CMyClientNode *owner = (CMyClientNode*)data;

	if (events & BEV_EVENT_CONNECTED) 
    {
		mmp_printf("EventCb:BEV_EVENT_CONNECTED...\n");
    	return;
	} 
    else if (events & BEV_EVENT_EOF) 
    {
		mmp_printf("EventCb:BEV_EVENT_EOF...\n");
		
		//bufferevent_free(bev);
		
		COMMU_PACK_HEAD inner_msg;
		inner_msg.cmd_type	= INNER_MSG_CMD_TYPE;
		inner_msg.dev_type	= INNER_MSG_CLIENT_CLOSED;		
		CIXDeviceServer* pserver = (CIXDeviceServer*)owner->m_owner;
		pserver->push_inner_msg_rcv_queue((void*)pserver,(char*)&inner_msg, sizeof(inner_msg),owner, 0);
	} 
    else if (events & BEV_EVENT_ERROR) 
    {
		mmp_printf("EventCb:BEV_EVENT_ERROR...\n");

		//bufferevent_free(bev);
		
		COMMU_PACK_HEAD inner_msg;
		inner_msg.cmd_type	= INNER_MSG_CMD_TYPE;
		inner_msg.dev_type	= INNER_MSG_CLIENT_ERROR;		
		CIXDeviceServer* pserver = (CIXDeviceServer*)owner->m_owner;
		pserver->push_inner_msg_rcv_queue((void*)pserver,(char*)&inner_msg, sizeof(inner_msg),owner, 0);
	}
	else
	{
		mmp_printf("EventCb:BEV_EVENT_ERROR_OTHERS...\n");

		//bufferevent_free(bev);
		
		COMMU_PACK_HEAD inner_msg;
		inner_msg.cmd_type	= INNER_MSG_CMD_TYPE;
		inner_msg.dev_type	= INNER_MSG_CLIENT_ERROR_OTHERS;		
		CIXDeviceServer* pserver = (CIXDeviceServer*)owner->m_owner;
		pserver->push_inner_msg_rcv_queue((void*)pserver,(char*)&inner_msg, sizeof(inner_msg), owner, 0);
	}
}
