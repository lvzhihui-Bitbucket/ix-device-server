
#ifndef _DBACCOUNT_H_
#define _DBACCOUNT_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <iostream>
#include <string>  
using std::string;

#include <utility>
#include <map>
using namespace std;

#include "main.h"
#include "threadpool.h"

typedef multimap<string,CMyClientNode*>     MULTIMAP_STR2PTR;
typedef map<CMyClientNode*,string>     		MAP_PTR2STR;

class CDBAcounts
{
//private:
public:
	MAP_PTR2STR			m_accounts_unregister;
    MULTIMAP_STR2PTR	m_accounts_registered;
	MAP_PTR2STR			m_accounts_tobekilled;
public:
    CDBAcounts();
    ~CDBAcounts();

	pthread_mutex_t     m_unregister_lock;
	pthread_mutex_t     m_registered_lock;
	pthread_mutex_t     m_tobekilled_lock;

	int unregister_add_one_account_node( CMyClientNode* ptr );
	int unregister_del_one_account_node( CMyClientNode* ptr, bool del_node );
	int unregister_del_all_account_node();
    int unregister_timeout_process();

	int registered_add_one_account_node( CMyClientNode* ptr );
	int registered_del_one_account_node( CMyClientNode* ptr );
	int registered_del_all_account_node();
    int registered_timeout_process();

	int tobekilled_add_one_account_node( CMyClientNode* ptr );
	int tobekilled_del_one_account_node( CMyClientNode* ptr );
	int tobekilled_del_all_account_node();
    int tobekilled_timeout_process();

	int modify_one_account_relation_info( CMyClientNode* pconn );
	int release_one_account_relation_info( CMyClientNode* pconn, bool lock_en );		

	int get_conn_ptr_from_one_account( string account, CMyClientNode** ppconn, int max );
	int update_one_account_relation_info( CMyClientNode* pconn );

	void print_all_conn_info();
};

#endif

